package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.LoginPage;

public class LoginTest extends BaseTest {


    @Test
    void shouldDisplayInvalidEmailAlertWrongEmailIsProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("xxxxxx@xxx.pl", "Test123!");
        Thread.sleep(5000);
        Assertions.assertTrue(loginPage.isAuthAlertDisplayed());
        Assertions.assertTrue(loginPage.getAuthAlertText().contains("Invalid Email or Password"));
    }

    @Test
    void shouldWelcomeUserTextIsProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("user@phptravels.com","demouser");
        Thread.sleep(5000);
        Assertions.assertTrue(loginPage.getWelcomeUserText().contains("Hi, Demo User"));
    }

}
