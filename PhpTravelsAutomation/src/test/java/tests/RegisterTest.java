package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.LoginPage;
import pageobjects.RegisterPage;
import utils.RandomUser;

public class RegisterTest extends BaseTest {

    @Test
    void shouldDisplayRegisterPage() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        Assertions.assertTrue(registerPage.getRegisterPage());

    }
    @Test
    void shouldRegisterUserWhenAllMandatoryDataProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        RandomUser randomUser = new RandomUser();
        registerPage.register(randomUser);
        Assertions.assertTrue(loginPage.getWelcomeUserText().contains("Hi, " + (randomUser.firstName + " " + randomUser.lastName)+ ""));
    }

    @Test
    void shouldDisplayedErrorMessageWhenExistEmailProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        RandomUser randomUser = new RandomUser();
        randomUser.email = "user@phptravels.com";
        registerPage.register(randomUser);
        Assertions.assertTrue(registerPage.getErrorMessageText().contains("Email Already Exists."));
    }
    @Test
    void shouldDisplayedErrorMessageWhenWrongEmailAddressWasProvided() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        RandomUser randomUser = new RandomUser();
        randomUser.email = "testtesttest";
        registerPage.register(randomUser);
        Thread.sleep(5000);
        Assertions.assertTrue(registerPage.getErrorMessageText().contains("The Email field must contain a valid email address."));
    }
    @Test
    void shouldDisplayedErrorMessageWhenPasswordsDontMatch() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        RandomUser randomUser = new RandomUser();
        randomUser.password = "testtesttest";
        randomUser.confirmPassword = "Qwas1234!";
        registerPage.register(randomUser);
        Thread.sleep(5000);
        Assertions.assertTrue(registerPage.getErrorMessageText().contains("Password not matching with confirm password."));
    }
    @Test
    void shouldDisplayedErrorMessageWhenPasswordIsTooShort() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        RegisterPage registerPage = loginPage.goToRegister();
        RandomUser randomUser = new RandomUser();
        randomUser.password = "xxx";
        randomUser.confirmPassword = "xxx";
        registerPage.register(randomUser);
        Thread.sleep(5000);
        Assertions.assertTrue(registerPage.getErrorMessageText().contains("The Password field must be at least 6 characters in length."));
    }
}
