package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.HomePage;
import pageobjects.LoginPage;

public class HomePageTest extends BaseTest {
    @Test
    void changeLanguageToFrench() throws InterruptedException {
        HomePage homepage = new HomePage(driver, wait);
        homepage.open();
        homepage.changeLanguage();
        Thread.sleep(5000);
        Assertions.assertTrue(homepage.frenchPhoneNumber().contains("Appeler maintenant"));
    }
}
