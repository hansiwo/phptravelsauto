package tests;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pageobjects.BasePage;
import pageobjects.LoginPage;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LogoutTest extends BaseTest{

    @Test
    void shouldlogoutUserWhenLogoutButtonIsClicked() throws InterruptedException {
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.open();
        loginPage.login("user@phptravels.com","demouser");
        Thread.sleep(5000);
        loginPage.clickOnSignOutButton();
        Assertions.assertTrue(loginPage.loginPageAreDisplayed());
    }

}
