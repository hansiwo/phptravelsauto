package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.w3c.dom.html.HTMLTitleElement;
import utils.RandomUser;

public class RegisterPage extends BasePage{

    String url = "https://phptravels.net/register";

    @FindBy(name = "firstname")
    WebElement firstName;

    @FindBy(name = "lastname")
    WebElement lastName;

    @FindBy(name = "phone")
    WebElement mobileNumber;

    @FindBy(name = "email")
    WebElement email;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(name = "confirmpassword")
    WebElement confirmPassword;

    @FindBy(className = "signupbtn")
    WebElement signUpButton;

    @FindBy(className = "alert-danger")
    WebElement errorMessage;

    public void register(RandomUser user) {
        firstName.sendKeys(user.firstName);
        lastName.sendKeys(user.lastName);
        mobileNumber.sendKeys(user.phone);
        email.sendKeys(user.email);
        password.sendKeys(user.password);
        confirmPassword.sendKeys(user.confirmPassword);
        signUpButton.click();
    }
    public RegisterPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    public String getErrorMessageText() {
        wait.until(ExpectedConditions.visibilityOf(errorMessage));
        return errorMessage.getText();
    }
    public boolean getRegisterPage() {
        wait.until(ExpectedConditions.visibilityOf(firstName));
        return firstName.isDisplayed();
    }
}
