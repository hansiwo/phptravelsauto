package pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage extends BasePage{

    final String LOGIN_URL = MAIN_URL + "/home";

    @FindBy(id = "dropdownLangauge")
    WebElement languages;

    @FindBy(id = "fr")
    WebElement french;

    @FindBy(className = "navbar-phone")
    WebElement contactNumber;


    public HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    public void changeLanguage() {
        wait.until(ExpectedConditions.elementToBeClickable(languages));
        languages.click();
        french.click();
    }
    public String frenchPhoneNumber() {
        wait.until(ExpectedConditions.visibilityOf(contactNumber));
        return contactNumber.getText();
    }
}
