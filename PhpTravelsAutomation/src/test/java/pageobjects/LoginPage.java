package pageobjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {

    final String LOGIN_URL = MAIN_URL + "/login";


    @FindBy(name = "username")
    WebElement email;

    @FindBy(name = "password")
    WebElement password;

    @FindBy(className = "alert-danger")
    WebElement authAlert;

    @FindBy(className = "text-align-left")
    WebElement welcomeUserText;

    @FindBy(className = "bx-user")
    WebElement userMenuButton;

    @FindBy(linkText = "Logout")
    WebElement logoutButton;

    @FindBy(className = "btn-success")
    WebElement signInButton;


    public LoginPage(WebDriver driver, WebDriverWait wait){
        super(driver,wait);
    }

    public void login(String emailValue, String passwordValue) {
        wait.until(ExpectedConditions.visibilityOf(email));
        email.sendKeys(emailValue);
        password.sendKeys(passwordValue);
        password.sendKeys(Keys.ENTER);
    }
    public boolean isAuthAlertDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.isDisplayed();
    }
    public String getAuthAlertText() {
        wait.until(ExpectedConditions.visibilityOf(authAlert));
        return authAlert.getText();
    }
    public String getWelcomeUserText() {
        wait.until(ExpectedConditions.visibilityOf(welcomeUserText));
        return welcomeUserText.getText();
    }
    public void clickOnSignOutButton() {
        wait.until(ExpectedConditions.elementToBeClickable(userMenuButton));
        userMenuButton.click();
        logoutButton.click();
    }
    public boolean loginPageAreDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(email));
        return email.isDisplayed();
    }
    public RegisterPage goToRegister(){
        wait.until(ExpectedConditions.visibilityOf(email));
        signInButton.click();
        return new RegisterPage(driver, wait);
    }

    public void open() {
        driver.get(LOGIN_URL);
    }
}
